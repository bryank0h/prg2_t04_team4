﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{

    public class FindMovie
    {
        public Meta meta { get; set; }
        public string type { get; set; }
        public string query { get; set; }
        public Result[] results { get; set; }
        public string[] types { get; set; }
    }

    public class Meta
    {
        public string operation { get; set; }
        public string requestId { get; set; }
        public float serviceTimeMs { get; set; }
    }

    public class Result
    {
        public string id { get; set; }
        public ImageFind image { get; set; }
        public int runningTimeInMinutes { get; set; }
        public string title { get; set; }
        public string titleType { get; set; }
        public int year { get; set; }
        public Principal[] principals { get; set; }
        public int episode { get; set; }
        public int season { get; set; }
        public string nextEpisode { get; set; }
        public Parenttitle parentTitle { get; set; }
        public string previousEpisode { get; set; }
        public int seriesStartYear { get; set; }
        public int numberOfEpisodes { get; set; }
        public string disambiguation { get; set; }
        public string legacyNameText { get; set; }
        public string name { get; set; }
        public Knownfor[] knownFor { get; set; }
        public string[] akas { get; set; }
    }

    public class ImageFind
    {
        public int height { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

    public class Parenttitle
    {
        public string id { get; set; }
        public ImageFind image { get; set; }
        public string title { get; set; }
        public string titleType { get; set; }
        public int year { get; set; }
    }

    public class Image1Find
    {
        public int height { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

    public class Principal
    {
        public string id { get; set; }
        public string legacyNameText { get; set; }
        public string name { get; set; }
        public string[] attr { get; set; }
        public int billing { get; set; }
        public string category { get; set; }
        public string[] characters { get; set; }
        public Role[] roles { get; set; }
        public string _as { get; set; }
        public string disambiguation { get; set; }
        public int endYear { get; set; }
        public int episodeCount { get; set; }
        public int startYear { get; set; }
    }

    public class Role
    {
        public string character { get; set; }
        public string characterId { get; set; }
    }

    public class Knownfor
    {
        public Cast[] cast { get; set; }
        public Summary summary { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string titleType { get; set; }
        public int year { get; set; }
        public Crew[] crew { get; set; }
        public string disambiguation { get; set; }
    }

    public class Summary
    {
        public string category { get; set; }
        public string[] characters { get; set; }
        public string displayYear { get; set; }
    }

    public class Cast
    {
        public int billing { get; set; }
        public string category { get; set; }
        public string[] characters { get; set; }
        public Role1[] roles { get; set; }
        public string _as { get; set; }
        public int endYear { get; set; }
        public int episodeCount { get; set; }
        public int startYear { get; set; }
        public string[] attr { get; set; }
    }

    public class Role1
    {
        public string character { get; set; }
        public string characterId { get; set; }
    }

    public class Crew
    {
        public string category { get; set; }
        public string job { get; set; }
        public int writerCategoryBilling { get; set; }
        public int writerTeamBilling { get; set; }
        public string[] freeTextAttributes { get; set; }
    }

    /////////////// Get Plot //////////////

    public class GetPlot
    {
        public string id { get; set; }
        public Base _base { get; set; }
        public Plot[] plots { get; set; }
    }

    public class Base
    {
        public string type { get; set; }
        public string id { get; set; }
        public ImagePlot image { get; set; }
        public string title { get; set; }
        public string titleType { get; set; }
        public int year { get; set; }
    }

    public class ImagePlot
    {
        public int height { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

    public class Plot
    {
        public string id { get; set; }
        public string text { get; set; }
        public string author { get; set; }
    }

    //////// Get Ratings ///////////

    public class GetRating
    {
        public string type { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string titleType { get; set; }
        public int year { get; set; }
        public bool canRate { get; set; }
        public Otherrank[] otherRanks { get; set; }
        public float rating { get; set; }
        public int ratingCount { get; set; }
        public Ratingshistograms ratingsHistograms { get; set; }
    }

    public class Ratingshistograms
    {
        public FemalesAgedUnder18 FemalesAgedunder18 { get; set; }
        public FemalesAged1829 FemalesAged1829 { get; set; }
        public AgedUnder18 Agedunder18 { get; set; }
        public Males Males { get; set; }
        public USUsers USusers { get; set; }
        public ImdbUsers IMDbUsers { get; set; }
        public FemalesAged45 FemalesAged45 { get; set; }
        public FemalesAged3044 FemalesAged3044 { get; set; }
        public MalesAged3044 MalesAged3044 { get; set; }
        public Aged3044 Aged3044 { get; set; }
        public NonUSUsers NonUSusers { get; set; }
        public Top1000Voters Top1000voters { get; set; }
        public MalesAgedUnder18 MalesAgedunder18 { get; set; }
        public MalesAged1829 MalesAged1829 { get; set; }
        public Aged45 Aged45 { get; set; }
        public ImdbStaff IMDbStaff { get; set; }
        public Aged1829 Aged1829 { get; set; }
        public MalesAged45 MalesAged45 { get; set; }
        public Females Females { get; set; }
    }

    public class FemalesAgedUnder18
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class FemalesAged1829
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram1 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram1
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class AgedUnder18
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram2 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram2
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Males
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram3 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram3
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class USUsers
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram4 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram4
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class ImdbUsers
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram5 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram5
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class FemalesAged45
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram6 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram6
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class FemalesAged3044
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram7 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram7
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class MalesAged3044
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram8 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram8
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Aged3044
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram9 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram9
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class NonUSUsers
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram10 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram10
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Top1000Voters
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram11 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram11
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class MalesAgedUnder18
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram12 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram12
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class MalesAged1829
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram13 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram13
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Aged45
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram14 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram14
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class ImdbStaff
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram15 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram15
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Aged1829
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram16 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram16
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class MalesAged45
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram17 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram17
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Females
    {
        public float aggregateRating { get; set; }
        public string demographic { get; set; }
        public Histogram18 histogram { get; set; }
        public int totalRatings { get; set; }
    }

    public class Histogram18
    {
        public int _1 { get; set; }
        public int _2 { get; set; }
        public int _3 { get; set; }
        public int _4 { get; set; }
        public int _5 { get; set; }
        public int _6 { get; set; }
        public int _7 { get; set; }
        public int _8 { get; set; }
        public int _9 { get; set; }
        public int _10 { get; set; }
    }

    public class Otherrank
    {
        public string id { get; set; }
        public string label { get; set; }
        public int rank { get; set; }
        public string rankType { get; set; }
    }


}
