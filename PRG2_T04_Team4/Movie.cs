﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Movie
    {
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; } = new List<string>();
        public List<Screening> ScreeningList { get; set; } = new List<Screening>();
        public Movie() { }
        public Movie(string ti, int du, string cl, DateTime op, List<string> ge)
        {
            Title = ti;
            Duration = du;
            Classification = cl;
            OpeningDate = op;
            GenreList = ge;
        }
        public void AddGenre(string g)
        {
            GenreList.Add(g);
        }
        public void AddScreening(Screening sc)
        {
            ScreeningList.Add(sc);
        }
        public override string ToString()
        {
            string genreString = "";
            foreach (string g in GenreList)
            {
                genreString += g + ", ";
            }
            genreString = genreString.Substring(0, genreString.Length - 2);

            return $"Title: {Title}\nDuration: {Duration}\nClassification: {Classification}\nOpening Date: {OpeningDate}\nGenre: {genreString}";
        }

    }
}
