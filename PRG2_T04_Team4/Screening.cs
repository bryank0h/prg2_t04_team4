﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Screening : IComparable<Screening>
    {
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }
        public Screening() { }
        public Screening(int scN, DateTime scDT, string scT, Cinema ci, Movie mo)
        {
            ScreeningNo = scN;
            ScreeningDateTime = scDT;
            ScreeningType = scT;
            Cinema = ci;
            Movie = mo;
        }

        public int CompareTo(Screening s)
        {
            return (SeatsRemaining.CompareTo(s.SeatsRemaining));
        }

        public override string ToString()
        {
            return $"Screening No: {ScreeningNo}\nScreening Date & Time: {ScreeningDateTime}\nScreening Type: {ScreeningType}\nSeats Remaining: {SeatsRemaining}\n{Cinema}\n{Movie}";
        }
        

    }
}
