﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Cinema
    {
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }
        public Cinema() { }
        public Cinema(string na, int ha, int ca)
        {
            Name = na;
            HallNo = ha;
            Capacity = ca;
        }
        public override string ToString()
        {
            return $"Cinema Name: {Name}\nHall No: {HallNo}\nCapacity: {Capacity}";
        }
    }
}
