﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    abstract class Ticket
    {
        public Screening Screening { get; set; }
        public Ticket() { }
        public Ticket(Screening sc)
        {
            Screening = sc;
        }
        public abstract double CalculatePrice();
        public override string ToString()
        {
            return $"{Screening}";
        }
    }
}
