﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;      // For Lists
using System.IO;                       // For Read and Writing Files
using System.Threading.Tasks;          // For Tasks
using System.Net.Http;                 // For Web API
using System.Web;                      // For Encoding string into suitable format
using Newtonsoft.Json;                 // For Deserializing

namespace PRG2_T04_Team4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create lists for Movie, Cinema, Screening, Order
            List<Movie> movieList = new List<Movie>();
            List<Cinema> cinemaList = new List<Cinema>();
            List<Screening> screeningList = new List<Screening>();
            List<Order> orderList = new List<Order>();
            
            // Load Movie and Cinema, Screening and Order.
            LoadMovieAndCinema(movieList, cinemaList);
            LoadScreening(screeningList, movieList, cinemaList);
            LoadOrder(orderList, screeningList);

            // Start running MainMenu method.
            MainMenu(movieList, screeningList, cinemaList, orderList);
                
            // If user selected 0 from the MainMenu method, the program will end.
            Console.WriteLine("Goodbye!");                                
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// MAIN MENU METHODS //////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // Display Main Menu with options such as General, Screening, Order.
        static void MainMenu(List<Movie> movieList, List<Screening> screeningList, List<Cinema> cinemaList, List<Order> orderList)
        {  
            // Display Main Menu
            while (true)
            {
                Console.WriteLine("==========================================");
                Console.WriteLine("    Singa Cineplexes Ticketing System     ");
                Console.WriteLine("==========================================");
                Console.WriteLine("[1] General");
                Console.WriteLine("[2] Screening");
                Console.WriteLine("[3] Order");
                Console.WriteLine();
                Console.WriteLine("[0] Exit");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());                    
                    Console.WriteLine();
                    if (option == 1)
                    {
                        MainMenu_General(movieList, cinemaList, screeningList, orderList);
                    }
                    else if (option == 2)
                    {
                        MainMenu_Screening(movieList, cinemaList, screeningList, orderList);
                    }
                    else if (option == 3)
                    {
                        MainMenu_Order(movieList, cinemaList, screeningList, orderList);
                    }
                    else if (option == 0)
                        return; // Returns to main menu
                    else
                    {
                        Console.WriteLine("Please select from the options available.");
                        Console.WriteLine();
                    }
                }
                catch (FormatException) // If user types non-numeric characters e.g. letters
                {
                    Console.WriteLine("Please enter the option number (e.g. 1 -> General).");
                    Console.WriteLine();
                }
            }
        }
        
        // Display Menu with General options such as List Movies and List Movie Screenings.
        static void MainMenu_General(List<Movie> movieList, List<Cinema> cinemaList, List<Screening> screeningList, List<Order> orderList)
        {
            // Display General menu
            while (true)
            {
                Console.WriteLine("==========================================");
                Console.WriteLine("                  General                 ");
                Console.WriteLine("==========================================");
                Console.WriteLine("[1] List all Movies");
                Console.WriteLine("[2] List all Movie Screenings");
                Console.WriteLine("[3] Movie Information (Extracted from IMDB)");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());                                   
                    Console.WriteLine();
                    if (option == 1)
                    {
                        ListMovies(movieList);
                        break;
                    }                       
                    else if (option == 2)
                    {
                        ListMovieScreenings(movieList, null, true);
                        break;
                    }
                    else if (option == 3)
                    {
                        MainMenu_MovieInformation(movieList);
                        break;
                    }
                    else if (option == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please select from the options available.");
                        Console.WriteLine();
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter the option number (e.g. 1 -> List all Movies).");
                }
            }
        }

        // Display Menu with Screening options such as Add Movie Screening Session and Delete Screening Session.
        static void MainMenu_Screening(List<Movie> movieList, List<Cinema> cinemaList, List<Screening> screeningList, List<Order> orderList)
        {
            while (true)
            {
                Console.WriteLine("==========================================");
                Console.WriteLine("                 Screening                ");
                Console.WriteLine("==========================================");
                Console.WriteLine("[1] Add Movie Screening Session");
                Console.WriteLine("[2] Delete Movie Screening Session");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (option == 1)
                    {
                        AddMovieScreeningSession(movieList, cinemaList, screeningList);
                        ScreeningFileWriting(screeningList);    // After creating an screening, write all data (via StreamWriter) into Screening.csv
                        break;
                    }
                    else if (option == 2)
                    {
                        DeleteMovieScreeningSession(screeningList, orderList);
                        ScreeningFileWriting(screeningList);    // After deleting an screening, write all data (via StreamWriter) into Screening.csv
                        break;
                    }
                    else if (option == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please select from the options available.");
                        Console.WriteLine();
                    }
                }
                catch (FormatException)     // If user types non-numeric characters e.g. letters
                {
                    Console.WriteLine("Please enter the option number (e.g. 1 -> List all Movies)."); 
                }
            }
        }

        // Display Menu with Order options such as Order Movie Tickets and Cancel Ticket Order.
        static void MainMenu_Order(List<Movie> movieList, List<Cinema> cinemaList, List<Screening> screeningList, List<Order> orderList)
        {
            // Display Order menu
            while (true)
            {
                Console.WriteLine("==========================================");
                Console.WriteLine("                   Order                  ");
                Console.WriteLine("==========================================");
                RecommendMovies(screeningList);
                Console.WriteLine("[1] Order Movie Tickets");
                Console.WriteLine("[2] Cancel Ticket Order");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (option == 1)
                    {
                        OrderMovieTickets(movieList, cinemaList, orderList);
                        OrderFileWriting(orderList);    // After creating an order, write all data (via StreamWriter) into Order.csv
                        break;
                    }
                    else if (option == 2)
                    {
                        CancelTicketOrder(screeningList, orderList);
                        OrderFileWriting(orderList);    // After deleting an order, write all data (via StreamWriter) into Order.csv
                        break;
                    }
                    else if (option == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please select from the options available.");
                        Console.WriteLine();
                    }
                }
                catch (FormatException)     // If user types non-numeric characters e.g. letters
                {
                    Console.WriteLine("Please enter the option number (e.g. 1 -> Order Movie Tickets).");
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// LOAD CSV FILES METHODS //////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // 1. LOAD MOVIE AND CINEMA DATA
        // Load Movie.csv and Cinema.csv, create movie objects and cinema objects, and add them to their respective lists.
        static void LoadMovieAndCinema(List<Movie> mList, List<Cinema> cList)
        {
            // Load Movie.csv
            using (StreamReader sr = new StreamReader("Movie.csv"))
            {
                string line = sr.ReadLine();
                string[] data;               
                while ((line = sr.ReadLine()) != null)
                {
                    data = line.Split(",");
                    List<string> genreList = new List<string>();
                    string[] genreArray = data[2].Split("/");
                    foreach (string g in genreArray)
                        genreList.Add(g);
                    mList.Add(new Movie(data[0], Convert.ToInt32(data[1]), data[3], DateTime.Parse(data[4]), genreList));
                }
            }
            // Load Cinema.csv
            using (StreamReader sr = new StreamReader("Cinema.csv"))
            {
                string line = sr.ReadLine();
                string[] data;
                while ((line = sr.ReadLine()) != null)
                {
                    data = line.Split(",");
                    cList.Add(new Cinema(data[0], Convert.ToInt32(data[1]), Convert.ToInt32(data[2])));
                }
            }
        }

        // 2. LOAD SCREENING DATA
        // Read Screening.csv and add Screening objects to screening list.
        static void LoadScreening(List<Screening> sList, List<Movie> mList, List<Cinema> cList)
        {
            // Load Screening.csv
            using (StreamReader sr = new StreamReader("Screening.csv"))
            {
                string s = sr.ReadLine();
                int screeningNo = 1001; // Default value is 1001;
                while ((s = sr.ReadLine()) != null)
                {
                    string[] data = s.Split(",");
                    DateTime datetime = DateTime.Parse(data[0]);
                    Cinema cinema = FindCinema(cList, data[2], Convert.ToInt32(data[3]));
                    Movie movie = FindMovie(mList, data[4]);
                    Screening screening = new Screening(screeningNo, datetime, data[1], cinema, movie);
                    screening.SeatsRemaining = cinema.Capacity;
                    movie.AddScreening(screening);
                    sList.Add(screening);
                    screeningNo += 1;       
                }
            }
        }

        // EXTRA
        // Read Order.csv, create Order objects and add them to order list. Updates seats remaining in Screening class.
        static void LoadOrder(List<Order> oList, List<Screening> sList)
        {
            using (StreamReader sr = new StreamReader("Order.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    string[] data = s.Split(',');
                    Order order = new Order(Convert.ToInt32(data[0]), DateTime.Parse(data[1]));
                    order.Amount = Convert.ToDouble(data[2]);
                    order.Status = data[3];
                    string[] ticketList = data[4].Split('-');                                               // Each ticket is split by '-' in the csv file.
                    for (int ticketNo = 0; ticketNo < ticketList.Length; ticketNo++)
                    {
                        string[] ticketDetail = ticketList[ticketNo].Split('/');                            // Each detail in each ticket is split with '/'. Values not applicable to student/seniorcitizen/adult are "null". e.g. Student does not have year of birth so its "null".
                        Screening screening = FindScreening(sList, Convert.ToInt32(ticketDetail[0]));       // Locate Screening object using screening number.
                        if (ticketDetail[1] != "null")       // If level of study is not "null".
                        {
                            order.AddTicket(new Student(screening, ticketDetail[1]));
                        }
                        else if (ticketDetail[2] != "null")  // If year of birth is not "null".
                        {
                            order.AddTicket(new SeniorCitizen(screening, Convert.ToInt32(ticketDetail[2])));
                        }
                        else if (ticketDetail[3] != "null")  // If popcorn offer is not "null".
                        {
                            bool popcornOffer;
                            if (Convert.ToInt32(ticketDetail[3]) == 1)  // true = 1, false = 0
                                popcornOffer = true;
                            else
                                popcornOffer = false;
                            order.AddTicket(new Adult(screening, popcornOffer));
                        }
                        screening.SeatsRemaining -= 1;    // Update seats remaining
                    }
                    oList.Add(order);
                }
            }
        }

        // EXTRA
        // Method used to find cinema from cinema list, returns the cinema object that matches the cinema name as well as the cinema hall number that was passed into the method.
        static Cinema FindCinema(List<Cinema> cList, string cName, int cHall)
        {

            foreach (Cinema c in cList)
            {
                if (c.Name == cName && c.HallNo == cHall)
                    return c;
            }
            return null;
        }

        // EXTRA
        // Method used to find movie from movie list, returns the movie object that matches the movie title that was passed into the method.
        static Movie FindMovie(List<Movie> mList, string mTitle)
        {
            foreach (Movie m in mList)
            {
                if (m.Title == mTitle)
                    return m;
            }
            return null;
        }

        // EXTRA
        // Method used to find screening from screening list, returns the screening object that matches the screening number that was passed into the method.
        static Screening FindScreening(List<Screening> sList, int sNo)
        {
            foreach (Screening s in sList)
            {
                if (s.ScreeningNo == sNo)
                    return s;               
            }
            return null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// GENERAL METHODS - LIST MOVIES & SCREENING //////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // 3. LIST ALL MOVIES
        // Displays all Movies available.
        static void ListMovies(List<Movie> mList)
        {
            Console.WriteLine("Information of all movies:\n");
            Console.WriteLine("     {0,-30} {1,-18} {2,-17} {3,-15} {4,-40}", "Title", "Duration (mins)", "Classification", "Opening Date", "Genre");   // Heading
            List<string> genreList;
            int index = 1; // For numbering of movie when printing.
            foreach (Movie m in mList)
            {
                genreList = m.GenreList;  // Get genrelist of each movie
                string genreString = "";
                foreach (string g in genreList)
                {
                    genreString += g + ", ";
                }
                genreString = genreString.Substring(0, genreString.Length - 2);
                Console.WriteLine("[{0, 2}] {1,-30} {2,-18} {3,-17} {4,-15} {5,-40}", index, m.Title, m.Duration, m.Classification, m.OpeningDate.ToString("dd/MM/yy"), genreString);
                index += 1;
            }
            Console.WriteLine();
        }      

        // LIST MOVIE SCREENINGS
        // Displays Screenings of the Movie Selected. Returns Movie selected. Allows for extra parameter to skip asking for movie and immediately list screenings, if a movie is passed in the method. Another parameter is added to specify if sorting is an option or not. The last parameter is added to filter out the screening sessions that have already been screened.
        static Movie ListMovieScreenings(List<Movie> mList, Movie movie = null, bool sortingAllowed = false, bool showScreenedSessions = true)
        {
            if (movie == null)
            {
                movie = GetMovieSelected(mList);   // Retrieves movie selected
                Console.WriteLine();                
            }
            if (movie == null)  // If retrieved movie is null, returns to main menu.
                return null;
            List<Screening> sList = movie.ScreeningList;
            Console.WriteLine("Screenings for: {0}", movie.Title);
            if (sList.Count != 0)
            {
                Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", "Screening No", "Screening Type", "Screening Date", "Screening Time", "Seats Remaining", "Cinema", "Hall No");  // Heading
                foreach (Screening s in sList)
                {
                    if (!showScreenedSessions)
                    {
                        if (s.ScreeningDateTime > DateTime.Now)
                            Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
                    }
                    else
                        Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
                }
                Console.WriteLine();
                if (sortingAllowed == true)
                    DisplayScreeningOrderByAvailableSeats(sList);  // For sorting of screening by seats, and group in many ways.
            }
            else
                Console.WriteLine("Sorry, there are no screenings for the selected movie.");        
            Console.WriteLine();           
            return movie;
        }

        // EXTRA
        // Return Movie selected to any method that needs it.
        static Movie GetMovieSelected(List<Movie> mList)
        {
            while (true)
            {
                ListMovies(mList);
                Movie movie;
                Console.Write("Please select a movie by its index (or enter 0 to return to Main Menu): ");
                try
                {
                    int movieindex = Convert.ToInt32(Console.ReadLine());
                    if (movieindex == 0)
                        return null;
                    int noOfMovies = mList.Count;
                    if (movieindex > noOfMovies || movieindex < 1)
                        throw new Exception();
                    movie = mList[movieindex - 1];
                    return movie;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please key in the movie index from the [], located at the left side of each movie.");
                }
                catch (Exception) // If user keys in a non-existent index.
                {
                    Console.WriteLine("Please select a movie index within the movies available.");
                }
                Console.WriteLine();
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// SCREENING METHODS - LEE WEI JUN NICHOLAS //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // 5. ADD A MOVIE SCREENING SESSION
        // Add Screening Session to the universal screening list as well as the ScreeningList in the movie selected.
        static void AddMovieScreeningSession(List<Movie> mList, List<Cinema> cList, List<Screening> sList)
        {
            bool addScreeningSatisfied = false;
            Movie m;
            DateTime sDT;
            Cinema cinema = null;
            bool returnMenu = false;
            bool returnDateTimeSelector = false;
            bool returnCinemaSelector = false;        
            string sType;

            while (true)
            {
                returnMenu = false;
                m = GetMovieSelected(mList);        // 1. List movies and prompt user to select movie
                if (m == null)
                {
                    Console.WriteLine();
                    return;
                }

                // 3. Prompt user to enter a screening type [2D/3D]
                while (true)
                {                    
                    Console.Write("Please enter a screening type [2D/3D] (or leave blank to return to previous menu): ");

                    sType = Console.ReadLine().ToUpper();
                    Console.WriteLine();

                    if (sType == "")
                    {
                        returnMenu = true;
                        break;
                    }
                    else if (sType == "2D" || sType == "3D")
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid option entered, please try again!");
                        Console.WriteLine();
                    }
                }
                
                if (returnMenu)
                {
                    Console.WriteLine("Returning to previous menu...");
                    Console.WriteLine();
                    continue;
                }

                while (true)
                {
                    // 4. Prompt user to enter a screening date and time (check to see if the datetime entered is after the opening date of the movie)
                    string sDate;
                    string sTime;
                    sDT = new DateTime();

                    while (true)
                    {
                        Console.WriteLine("Note: You may leave all field(s) empty to exit current selection.");
                        Console.Write("Please enter a screening date [dd/mm/yy]: ");
                        sDate = Console.ReadLine();
                        Console.Write("Please enter a screening time in 24 hour format [hh:mm]: ");
                        sTime = Console.ReadLine();
                        Console.WriteLine();

                        if (sDate != "" && sTime != "")
                        {
                            try
                            {
                                sDT = Convert.ToDateTime(sDate + " " + sTime);

                                if (sDT.Subtract(m.OpeningDate).Days >= 0)
                                    break;
                                else
                                {
                                    Console.WriteLine("Date entered is before the opening date of movie. Please select a date after the opening date of the movie [{0}]", m.OpeningDate);
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Invalid format of date and time entered! Please try again.");
                            }
                            Console.WriteLine();
                        }
                        else if (sDate + sTime == "")
                        {
                            returnMenu = true;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Please fill in all the required fields!");
                            Console.WriteLine();
                        }
                    }

                    if (returnMenu)
                    {
                        Console.WriteLine("Returning to previous menu...");                       
                        Console.WriteLine();
                        break;
                    }

                    // 5. List all cinema halls
                    List<Screening> movieScreeningList = m.ScreeningList;
                    cinema = null;
                    int cinemaIndex;

                    // 6. Prompt user to select a cinema hall (check to see if the cinema hall is available at the datetime entered in point 4) [need to consider the movie duration and cleaning time]
                    while (true)
                    {
                        while (true)
                        {
                            Console.WriteLine("Information of all cinemas:\n");     // Heading
                            Console.WriteLine("     {0,-20} {1,-15} {2,-10}", "Name", "Hall Number", "Capacity");
                            int index = 1;
                            foreach (Cinema c in cList)
                            {
                                Console.WriteLine("[{0, 2}] {1,-20} {2,-15} {3,-10}", index, c.Name, c.HallNo, c.Capacity);
                                index += 1;
                            }
                            Console.WriteLine();
                            Console.Write("Please select a cinema hall by its index (or enter 0 to return to previous menu): ");
                            try
                            {
                                cinemaIndex = Convert.ToInt32(Console.ReadLine());
                                if (cinemaIndex == 0)
                                {
                                    Console.WriteLine();
                                    returnMenu = true;
                                    break;
                                }
                                else if (cinemaIndex > cList.Count || cinemaIndex < 0)  
                                    throw new Exception();
                                else
                                {
                                    Console.WriteLine();
                                    cinema = cList[cinemaIndex - 1];
                                    break;
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter the index number of the cinema hall from the listed cinema halls.");
                                Console.WriteLine();
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Please select from the options available.");
                                Console.WriteLine();
                            }
                        }
                        if (returnMenu)
                        {
                            returnMenu = false;
                            Console.WriteLine("Returning to previous menu...");
                            Console.WriteLine();
                            break;
                        }

                        bool clashFlag = false;
                        foreach (Movie movie in mList)
                        {
                            List<Screening> screenList = m.ScreeningList;

                            foreach (Screening screen in screenList)
                            {
                                if (screen.Cinema == cinema)
                                {
                                    
                                    if (DateTime.Compare(screen.ScreeningDateTime.AddMinutes(m.Duration + 30), sDT) > 0)    // If date time entered clashes with cleaning time / other screening times.
                                    {
                                        clashFlag = true;
                                        string reason = $"Reason: Time entered clashes with cleaning time.";
                                        Console.WriteLine($"Movie screening session creation is unsuccessful.");
                                        if (DateTime.Compare(screen.ScreeningDateTime.AddMinutes(m.Duration), sDT) > 0)     // Updates reason if clash is due to screening time clash.
                                            reason = $"Reason: Time entered clashes with another movie screening.";            
                                        Console.WriteLine(reason);
                                        Console.WriteLine("\n============= Clash Details ==============" +
                                                $"\nScreening No: {screen.ScreeningNo}" +
                                                $"\nMovie Title: {screen.Movie.Title}" +
                                                $"\nCinema: {screen.Cinema.Name}, Hall {screen.Cinema.HallNo}" +
                                                $"\nScreening Date: { screen.ScreeningDateTime:d}" +
                                                $"\nScreening Time: {screen.ScreeningDateTime:T} to {screen.ScreeningDateTime.AddMinutes(screen.Movie.Duration):T}" +
                                                $"\nEnd of Cleaning Time: { screen.ScreeningDateTime.AddMinutes(screen.Movie.Duration + 30):T}" +
                                                "\n==========================================");
                                        break;
                                    }
                                }
                            }
                            if (clashFlag)
                                break;
                            else
                                addScreeningSatisfied = true;
                        }
                        if (clashFlag) // Options if a clash occurs
                        {
                            while (true)
                            {
                                Console.WriteLine("Options:");
                                Console.WriteLine("[1] Return to Date & Time Selection");
                                Console.WriteLine("[2] Return to Cinema Hall Selection");
                                Console.WriteLine();
                                Console.WriteLine("[0] Return to Main Menu");
                                Console.WriteLine("------------------------------------------");
                                Console.Write("Please select an option: ");
                                try
                                {
                                    int option = Convert.ToInt32(Console.ReadLine());
                                    if (option == 0)
                                    {
                                        Console.WriteLine();
                                        return;
                                    }
                                    else if (option == 1)
                                    {
                                        returnDateTimeSelector = true;
                                        Console.WriteLine();
                                        break;
                                    }
                                    else if (option == 2)
                                    {
                                        returnCinemaSelector = true;
                                        Console.WriteLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please select from the options available.");
                                        Console.WriteLine();
                                    }
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Please enter the option number (e.g. 1 -> Return to Date & Time Selection).");
                                    Console.WriteLine();
                                }
                            }
                        }
                        if (returnCinemaSelector)
                        {
                            returnCinemaSelector = false;
                            continue;
                        }
                        else if (returnDateTimeSelector || addScreeningSatisfied)
                            break;
                    }
                    if (returnDateTimeSelector)
                    {
                        returnDateTimeSelector = false;
                        continue;
                    }
                    else if (addScreeningSatisfied)
                        break;
                }

                if (addScreeningSatisfied)  // 7. Create a Screening object
                {
                    Screening screening = new Screening(sList[sList.Count - 1].ScreeningNo + 1, sDT, sType, cinema, m);
                    screening.SeatsRemaining = cinema.Capacity;
                    sList.Add(screening);
                    m.ScreeningList.Add(screening);
                    Console.WriteLine("Movie screening session has been successfully created."); // 8. Status
                    Console.WriteLine();
                    ListMovieScreenings(mList, m);
                    Console.WriteLine();
                    return;
                }                          
            }
        }

        // 6. DELETE A MOVIE SCREENING SESSION
        // Deletes selected screening (based on screenings with no tickets sold)
        static void DeleteMovieScreeningSession(List<Screening> sList, List<Order> oList)
        {
            // 1. List all movie screening sessions that have not sold any tickets
            List<Screening> scnToDelList = new List<Screening>();
            foreach(Screening scn in sList)
            {
                if (scn.SeatsRemaining == scn.Cinema.Capacity)
                {
                    scnToDelList.Add(scn);
                }
            }
            scnToDelList = DisplayScreeningGroupByTitle(scnToDelList);
            Console.WriteLine();

            // 2. Prompt user to select a session
            int scnNoToDel;
            Screening screeningToDelete = null;
            while (true)
            {
                Console.Write("Please select a movie session by its screening number (or enter 0 to return to Main Menu): ");
                try
                {
                    scnNoToDel = Convert.ToInt32(Console.ReadLine());
                    if (scnNoToDel == 0)
                    {
                        Console.WriteLine("Returning to main menu...");
                        Console.WriteLine();
                        return;
                    }

                    for (int i = 0; i < scnToDelList.Count; i++) // Find screening from the list of screenings with no tickets sold
                    {
                        if (scnToDelList[i].ScreeningNo == scnNoToDel)
                        {
                            screeningToDelete = scnToDelList[i];
                            break;
                        }
                    }

                    if (screeningToDelete != null)
                    {
                        for (int i = 0; i < sList.Count; i++)
                        {
                            if (sList[i].ScreeningNo > screeningToDelete.ScreeningNo)   // All screenings with screening numbers more than the deleted screening will decrement by 1.
                                sList[i].ScreeningNo -= 1;
                        }
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid screening number entered, please try again!");
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid format of screening number entered!");
                }
                Console.WriteLine();
            }

            // 3. Remove the movie screening from all screening lists
            sList[sList.IndexOf(screeningToDelete)].Movie.ScreeningList.Remove(screeningToDelete);
            sList.Remove(screeningToDelete);

            // 4. Display the status of the removal
            Console.WriteLine("The selected movie screening session was successfully removed.");
            Console.WriteLine("Returning to main menu...");
            Console.WriteLine();

        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// ORDER METHODS - BRYAN KOH ////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // 7. ORDER MOVIE TICKET/S
        // After the ordering process, adds the order into the order list (found in this program.cs).
        static void OrderMovieTickets(List<Movie> mList, List<Cinema> cList, List<Order> oList)
        {
            // Variables needed to store movie and screening.
            Movie movie = null;
            List<Screening> sList = new List<Screening>();
            List<Screening> AvailableScreeningsList = new List<Screening>();
            Screening screening = null;
            int noOfTickets = 0;

            // Variables needed for checking age.
            List<List<string>> ageRequirementList = new List<List<string>>();
            ageRequirementList.Add(new List<string> { "G", "0" });
            ageRequirementList.Add(new List<string> { "PG13", "13" });
            ageRequirementList.Add(new List<string> { "NC16", "16" });
            ageRequirementList.Add(new List<string> { "M18", "18" });
            ageRequirementList.Add(new List<string> { "R21", "21" });
            List<int> ticketsAgeList = new List<int>();
            int ageRequired = 0;
            bool ageAsked = false;

            while (true)
            {
                if (noOfTickets > 0) // Check if order is voided or not. (e.g. age requirement not met)
                    break;
                while (true)
                {
                    // 1. List All Movies
                    // 2. Prompt user to select a movie
                    // 3. List all movie screenings of the selected movie
                    movie = ListMovieScreenings(mList, null, false, false);  
                    if (movie == null) // If movie returned is null, returns to main menu.
                        return;
                    sList = movie.ScreeningList;   
                    foreach (Screening s in sList)
                    {
                        if (s.ScreeningDateTime > DateTime.Now)
                            AvailableScreeningsList.Add(s); // Stores screenings that are not before or equal to the current time
                    }
                    char anotherMovie;
                    if (AvailableScreeningsList.Count == 0) // If screening list has no screenings, prompt if he/she wants to select another movie.
                    {
                        while (true)
                        {
                            Console.Write("Would you like to select another movie? [Y/N]: ");
                            try
                            {
                                anotherMovie = Convert.ToChar(Console.ReadLine());
                                anotherMovie = Char.ToUpper(anotherMovie);
                                if (anotherMovie == 'Y')
                                    break;
                                else if (anotherMovie == 'N')
                                {
                                    Console.WriteLine();
                                    return;
                                }
                                else
                                    throw new FormatException();
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please select Yes[Y] or No[N].");
                            }
                        }
                    }
                    else
                        break;
                }
                while (true)
                {
                    while (true)
                    {
                        // 4. Prompt user to select movie screening
                        Console.Write("Please select a movie screening using the movie screening number (or enter 0 to return to Main Menu): ");
                        try
                        {
                            int screeningNo = Convert.ToInt32(Console.ReadLine());
                            if (screeningNo == 0) // If 0, return to main menu.
                            {
                                Console.WriteLine();
                                return;
                            }                       
                            foreach (Screening s in AvailableScreeningsList) 
                            {
                                if (s.ScreeningNo == screeningNo) // Check if screening number entered is part of the selected movie's screening list.
                                {
                                    screening = s;
                                    break;
                                }
                            }
                            if (screening == null)
                                throw new Exception();
                            break;
                        }
                        catch (FormatException) // Prevent user from entering a non-numerical character.
                        {
                            Console.WriteLine("Please enter a screening number (e.g. 1001).");
                            Console.WriteLine();
                        }
                        catch (Exception)  // Exception thrown if screening number entered is not part of the selected movie's screening list.
                        {
                            Console.WriteLine("Please enter a screening number from the listed screenings.");
                        }
                    }
                    bool validNumber = true;
                    bool anotherScreening = false;
                    bool anotherMovie = false;
                    while (true)
                    {
                        // 6. Prompt user to enter total number of tickets to order
                        Console.Write("Enter number of tickets to order: ");
                        try
                        {
                            noOfTickets = Convert.ToInt32(Console.ReadLine());
                            if (noOfTickets < 1)
                                throw new Exception();
                            if (noOfTickets > screening.SeatsRemaining) // Check if figure entered is more than the available seats for the screening
                            {
                                validNumber = false;
                                noOfTickets = 0;
                                while (true) // Options given if user tries to order tickets exceeding capacity of cinema.
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Sorry, the number of tickets requested has exceeded the number of seats remaining for the screening selected.");
                                    Console.WriteLine("[1] Purchase less tickets");
                                    Console.WriteLine("[2] View other screening sessions for the same movie");
                                    Console.WriteLine("[3] View other movies");
                                    Console.WriteLine();
                                    Console.WriteLine("[0] Return to Main Menu");
                                    Console.WriteLine("------------------------------------------");
                                    Console.Write("Please select an option: ");
                                    try
                                    {
                                        int option = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine();
                                        if (option == 1)
                                            break;
                                        else if (option == 2)
                                        {
                                            ListMovieScreenings(mList, movie);
                                            anotherScreening = true;
                                            break;
                                        }
                                        else if (option == 3)
                                        {
                                            anotherMovie = true;
                                            break;
                                        }
                                        else if (option == 0)
                                            return;
                                        else
                                        {
                                            Console.WriteLine("Please select from the options available.");
                                            Console.WriteLine();
                                        }
                                    }
                                    catch (FormatException) // Prevent user from enter non-numerical character.
                                    {
                                        Console.WriteLine("Please enter the option number (e.g. 1 -> Purchase less tickets).");
                                        Console.WriteLine();
                                    }
                                }
                            }
                            else
                                validNumber = true;
                            if (validNumber == true || anotherScreening == true || anotherMovie == true)
                                break;
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Please enter a number.");
                            Console.WriteLine();
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Input unaccepted. Please order a minimum of 1 ticket.");
                            Console.WriteLine();
                        }
                    }
                    if (anotherScreening == true)
                        continue;
                    // Prompt user if all ticket holders meet the movie classification requirements
                    if (validNumber == true)
                    {
                        List<string> classification = new List<string>();
                        foreach (List<string> classificationList in ageRequirementList)
                        {
                            if (classificationList[0] == movie.Classification)
                            {
                                classification = classificationList; // Get classification details.
                                break;
                            }
                        }
                        if (Convert.ToInt32(classification[1]) != 0) // If classification is not for everyone, prompt for their age.
                        {       
                            for (int i = 0; i < noOfTickets; i++)
                            {
                                bool ageCheck = false;
                                while (!(ageCheck))
                                {
                                    Console.WriteLine();
                                    Console.Write($"Please state the age of ticket holder {i + 1}: ");
                                    try
                                    {
                                        int age = Convert.ToInt32(Console.ReadLine());
                                        if (age >= 55)
                                        {
                                            // Auto sets people whose age > 55 to senior citizen ticket.
                                            Console.WriteLine($"Ticket for Ticket holder {i + 1} is automatically set to Senior Citizen ticket.");
                                            ticketsAgeList.Add(age);
                                            ageCheck = true;
                                        }
                                        else if (age >= Convert.ToInt32(classification[1]))
                                        {
                                            // People who meet movie age classification requirements
                                            ticketsAgeList.Add(age);
                                            ageCheck = true;
                                        }
                                        else
                                        {
                                            // If one person does not meet the age requirement, void the entire order.
                                            ticketsAgeList.Clear();
                                            while (true) // Options given if age requirement not met.
                                            {
                                                Console.WriteLine();
                                                Console.WriteLine($"Sorry, but people below the age of {classification[1]} are not allowed to watch the movie.");
                                                Console.WriteLine("[1] View other movies");
                                                Console.WriteLine("[2] Return to Main Menu");
                                                Console.WriteLine("------------------------------------------");
                                                Console.Write("Please select an option: ");
                                                try
                                                {
                                                    int option = Convert.ToInt32(Console.ReadLine());
                                                    Console.WriteLine();
                                                    if (option == 1)
                                                    {
                                                        noOfTickets = 0;
                                                        anotherMovie = true;
                                                        break;
                                                    }
                                                    else if (option == 2)
                                                        return;
                                                    else
                                                    {
                                                        Console.WriteLine("Please select from the options available.");
                                                    }
                                                }
                                                catch (FormatException)
                                                {
                                                    Console.WriteLine("Please enter the option number (e.g. 1 -> View other movies).");
                                                }
                                            }
                                        }
                                        break;
                                    }
                                    catch (FormatException)
                                    {
                                        Console.WriteLine("Please enter a number.");
                                    }
                                }                  
                            }
                            ageRequired = Convert.ToInt32(classification[1]);
                            ageAsked = true;

                        }
                    }
                    if (validNumber == true || anotherMovie == true)
                        break;
                }
            }
            Console.WriteLine();
            int newOrderNo = oList[oList.Count - 1].OrderNo + 1;
            Order order = new Order(newOrderNo, DateTime.Now); 
            order.Status = "Unpaid"; // 8. Created order object with the status "Unpaid"
            List<string> levelOfStudyList = new List<string> { "Primary", "Secondary", "Tertiary" };
            for (int ticketNo = 0; ticketNo < noOfTickets; ticketNo++) // Begin setting ticket type.
            {
                string ticketType = "";
                if (ticketsAgeList.Count != 0)
                {
                    if (ticketsAgeList[ticketNo] >= 55)
                        ticketType = "senior"; // Auto sets ticket type to senior if age requirement is met.
                }
                bool ticketTypeCondition = false;  // Boolean set so that only after input is valid, user can proceed to the next ticket.             
                while (!(ticketTypeCondition))
                {
                    if (ticketType == "")
                    {
                        // 9. Prompt user for a response depending on the type of ticket ordered.
                        // Senior option will not be available if the age is already asked earlier.
                        Console.Write($"Ticket {ticketNo + 1}: What type of ticket? [Student/Adult{(ageAsked ? "" : "/Senior")}]: ");                
                        ticketType = Console.ReadLine();
                        ticketType = ticketType.ToLower();
                        if (ageAsked && ticketType == "senior") // In case user tries to key in Senior.
                        {
                            Console.WriteLine($"Ticket holder {ticketNo + 1} is ineligible for senior ticket as he/she does not meet the age requirement of 55 years old and above.");
                            ticketType = "";
                        }
                    }                   
                    if (ticketType == "student")  // Student ticket
                    {
                        // Boolean variables made to check if Age and Level of Study is accurate.
                        bool PrimaryCondition = false;
                        bool SecondaryCondition = false;
                        bool TertiaryCondition = false;

                        if (ageAsked)
                        {
                            PrimaryCondition = ticketsAgeList[ticketNo] < 15;                                           // Primary School students cannot be 15 years old or above.
                            SecondaryCondition = ticketsAgeList[ticketNo] > 12 && ticketsAgeList[ticketNo] < 19;        // Secondary School students cannot be below 13 years old and cannot be above 18 years old.
                            TertiaryCondition = ticketsAgeList[ticketNo] > 15;                                          // Tertiary students cannot be below 16 years old.
                        }
                        while (true)
                        {
                            if (ageAsked)  // Display options accurate to the age of the ticket holder.
                                Console.Write($"Enter Level of Study [{(PrimaryCondition ? "Primary" : "")}{((ticketsAgeList[ticketNo] > 12 && PrimaryCondition) ? ", " : "")}{(SecondaryCondition ? "Secondary" : "")}{(TertiaryCondition && ticketsAgeList[ticketNo] < 19 ? ", " : "")}{(TertiaryCondition ? "Tertiary" : "")}] (or enter 0 to return to ticket type selection): ");
                            else
                                Console.Write($"Enter Level of Study [Primary, Secondary, Tertiary] (or enter 0 to return to ticket type selection): ");
                            string levelOfStudy = Console.ReadLine();
                            if (levelOfStudy == "0") // Returns to select ticket type.
                            {
                                ticketType = "";
                                break;
                            }
                            levelOfStudy = (levelOfStudy.Substring(0, 1)).ToUpper() + (levelOfStudy.Substring(1)).ToLower();
                            // Strict checking to ensure that Level of Study entered is correct and accurate.
                            if ((ageAsked == false && levelOfStudyList.Contains(levelOfStudy)) || (levelOfStudyList.Contains(levelOfStudy) && ((levelOfStudy == "Primary" && PrimaryCondition) || (levelOfStudy == "Secondary" && SecondaryCondition) || (levelOfStudy == "Tertiary" && TertiaryCondition))))
                            {
                                order.TicketList.Add(new Student(screening, levelOfStudy));
                                ticketTypeCondition = true;
                                screening.SeatsRemaining -= 1;
                                break;                     
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid level of study.");
                                Console.WriteLine();
                            }
                        }        
                    }
                    else if (ticketType == "senior") // Senior ticket
                    {                        
                        if (ageAsked) // If age has been asked already, then year of birth will be calculated and possible years will be presented for the user to select.
                        {
                            int yearOfBirth = Convert.ToInt32(DateTime.Now.ToString("yyyy")) - ticketsAgeList[ticketNo];
                            while (true)
                            {
                                Console.WriteLine($"Select Year of Birth for Ticket Holder {ticketNo + 1}: ");
                                Console.WriteLine($"[1] {yearOfBirth}");
                                Console.WriteLine($"[2] {yearOfBirth - 1}");
                                Console.WriteLine("------------------------------------------");
                                Console.Write("Please select an option: ");
                                try
                                {
                                    int option = Convert.ToInt32(Console.ReadLine());
                                    if (option == 1)
                                    {
                                        Console.WriteLine();
                                        order.TicketList.Add(new SeniorCitizen(screening, yearOfBirth));
                                        ticketTypeCondition = true;
                                        screening.SeatsRemaining -= 1;
                                        break;

                                    }
                                    else if (option == 2)
                                    {
                                        Console.WriteLine();
                                        order.TicketList.Add(new SeniorCitizen(screening, yearOfBirth - 1));
                                        ticketTypeCondition = true;
                                        screening.SeatsRemaining -= 1;
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please select from the options available.");
                                        Console.WriteLine();
                                    }

                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine($"Please enter the option number (e.g. 1 -> {yearOfBirth}).");
                                    Console.WriteLine();
                                }
                            }
                        }
                        else // If age has not been asked yet, prompt for year of birth of ticket holder.
                        {
                            while (true)
                            {
                                Console.Write($"Enter Year of Birth for ticket holder {ticketNo + 1} in YYYY format (or enter 0 to return to ticket type selection): ");
                                try
                                {
                                    int yearOfBirth = Convert.ToInt32(Console.ReadLine());
                                    if (yearOfBirth == 0)
                                    {
                                        ticketType = "";
                                        break;
                                    }
                                    if ((yearOfBirth.ToString()).Length != 4) // If entered year is not exactly 4 characters long, throw format exception.
                                        throw new FormatException();
                                    if ((DateTime.Now.Year - yearOfBirth) >= 55)
                                    {
                                        order.TicketList.Add(new SeniorCitizen(screening, yearOfBirth));
                                        ticketTypeCondition = true;
                                        screening.SeatsRemaining -= 1;
                                        Console.WriteLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Sorry, the ticket holder is ineligible for the Senior Citizen ticket as he/she does not meet the age requirement of 55 years old and above.");
                                        ticketType = "";
                                        Console.WriteLine();
                                    }
                                    break;
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Please enter the year of birth in the format YYYY (e.g. 1965).");
                                    Console.WriteLine();
                                }
                            }        
                        }                    
                    }
                    else if (ticketType == "adult") // Adult ticket
                    {
                        while (true)
                        {
                           
                            Console.Write("Popcorn offer? [Y/N] (or enter 0 to return to ticket type selection): ");
                            try
                            {
                                char popcornOffer = Convert.ToChar(Console.ReadLine());
                                if (popcornOffer == '0')
                                {
                                    ticketType = "";
                                    break;
                                }
                                popcornOffer = Char.ToUpper(popcornOffer);
                                if (popcornOffer == 'Y')
                                {
                                    order.TicketList.Add(new Adult(screening, true));
                                    ticketTypeCondition = true;
                                    screening.SeatsRemaining -= 1;
                                    break;
                                }
                                else if (popcornOffer == 'N')
                                {
                                    order.TicketList.Add(new Adult(screening, false));
                                    ticketTypeCondition = true;
                                    screening.SeatsRemaining -= 1;
                                    break;
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please select Yes[Y] or No[N].");
                                Console.WriteLine();
                            }
                        }
                    }
                    else
                    {
                        if (ageAsked)
                            Console.WriteLine("Please select between Student or Adult.");
                        else
                        {
                            Console.WriteLine("Please enter a valid ticket type.");
                            ticketType = "";
                        }                          
                    }
                    Console.WriteLine();
                }               
            }
            ticketsAgeList.Clear();
            double TotalPrice = 0.00;
            // Order Summary before payment
            Console.WriteLine("==========================================");
            Console.WriteLine("               Order Summary              ");
            Console.WriteLine("==========================================");
            Console.WriteLine($"Movie:          {movie.Title} ({screening.ScreeningType})");
            Console.WriteLine($"Cinema:         {screening.Cinema.Name}, Hall {screening.Cinema.HallNo}");
            Console.WriteLine($"Screening No:   {screening.ScreeningNo}");
            Console.WriteLine($"Screening Date: {screening.ScreeningDateTime:dd MMMM yyyy (dddd)}");          
            Console.WriteLine();
            for (int i = 0; i < order.TicketList.Count; i++)
            {               
                Console.WriteLine($"Ticket {i + 1}:");
                if (order.TicketList[i] is Student)
                {
                    Student s = (Student)order.TicketList[i];
                    Console.WriteLine("Type of ticket: Student");
                    Console.WriteLine($"Level of Study: {s.LevelOfStudy}");
                    Console.WriteLine($"Price: ${s.CalculatePrice():0.00}");
                    TotalPrice += s.CalculatePrice();
                }
                else if (order.TicketList[i] is SeniorCitizen)
                {
                    SeniorCitizen sc = (SeniorCitizen)order.TicketList[i];
                    Console.WriteLine("Type of ticket: Senior Citizen");
                    Console.WriteLine($"Year of Birth: {sc.YearOfBirth}");
                    Console.WriteLine($"Price: ${sc.CalculatePrice():0.00}");
                    TotalPrice += sc.CalculatePrice();
                }
                else if (order.TicketList[i] is Adult)
                {
                    Adult a = (Adult)order.TicketList[i];
                    Console.WriteLine("Type of ticket: Adult");
                    Console.WriteLine($"Popcorn Offer: {(a.PopcornOffer ? "Yes" : "No")}");
                    if (a.PopcornOffer)
                        Console.WriteLine($"Price: ${(a.CalculatePrice() - 3.00).ToString("0.00")} + $3.00 = ${a.CalculatePrice():0.00}");
                    else
                        Console.WriteLine($"Price: ${a.CalculatePrice():0.00}");
                    TotalPrice += a.CalculatePrice();
                }
                Console.WriteLine();
            }
            // 10. List amount payable.
            Console.WriteLine($"Total Price: ${TotalPrice:0.00}");
            Console.WriteLine("==========================================");
            Console.WriteLine("Press any key to make payment. (or press 'Esc' to exit/decline payment and return to Main Menu)");
            ConsoleKeyInfo clickedButton = Console.ReadKey(true); // 11. Prompt user to press any key to make payment.
            if (clickedButton.Key == ConsoleKey.Escape)
            {
                Console.WriteLine("Payment declined and current order has been voided.\nReturning to Main Menu...");
                Console.WriteLine();
                return;
            }
            else
            {
                // 12. Fill in the necessary details to the new order.
                order.Amount = TotalPrice;
                order.Status = "Paid"; // 13. Change order status to "Paid"
                oList.Add(order);
                Console.WriteLine("Payment completed.\nThank you for your order! Have a nice day!");
                Console.WriteLine();
            }
        }
        
        // 8. CANCEL ORDER OF TICKET
        // Updates seat remaining and changes order status to "Cancelled" for the order selected, followed by removing the order from the order list (found in this program.cs).
        static void CancelTicketOrder(List<Screening> sList, List<Order> oList)
        {
            Order order = null;
            while (true)
            {
                // 1. Prompt user for order number.
                Console.Write("Please enter order number (or enter 0 to return to main menu): ");
                try
                {
                    int orderNumber = Convert.ToInt32(Console.ReadLine());
                    if (orderNumber == 0)
                    {
                        Console.WriteLine();
                        return;
                    }                      
                    foreach (Order o in oList)
                    {
                        if (o.OrderNo == orderNumber) // 2. Retrieve the selected order
                        {
                            order = o;
                            break;
                        }                            
                    }
                    bool validScreening = true;
                    if (order != null)
                    {
                        if (DateTime.Compare(order.TicketList[0].Screening.ScreeningDateTime, DateTime.Now) <= 0) // 3. Check if the screening in the selected order is screened.
                            validScreening = false;
                    }
                    if (order == null || order.Status == "Cancelled" || validScreening == false) // If order cannot be found, or the order has been already cancelled, or the order is already screened, provide options.
                    {
                        while (true)
                        {                           
                            string declineReason = "Declined";
                            if (order == null)
                                declineReason = "Order does not exist.";
                            else if (order.Status == "Cancelled")
                                declineReason = "Order is already cancelled.";
                            else if (validScreening == false)
                                declineReason = "The screenings in the selected order have already been screened.";
                            Console.WriteLine($"Order cancellation is unsuccessful.\nReason: {declineReason}");
                            Console.WriteLine("[1] Enter another order number");
                            Console.WriteLine("[2] Return to Main Menu");
                            Console.WriteLine("------------------------------------------");
                            Console.Write("Please select an option: ");
                            try
                            {
                                int option = Convert.ToInt32(Console.ReadLine());
                                if (option == 1)
                                {
                                    Console.WriteLine();
                                    order = null;
                                    break;
                                }
                                else if (option == 2)
                                {
                                    Console.WriteLine();
                                    order = null;
                                    return;
                                }
                                else
                                {
                                    Console.WriteLine("Please select from the options available.");
                                    Console.WriteLine();
                                }                 
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter the option number (e.g. 1 -> Try again).");
                                Console.WriteLine();
                            }
                        }
                    }
                    else
                        break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a number (no characters or letters).");
                    Console.WriteLine();
                }
            }
            foreach (Ticket t in order.TicketList) // 4. Update seat remaining for the movie screening based on the selected order.
            {
                t.Screening.SeatsRemaining += 1;
            }
            order.Status = "Cancelled"; // 5. Change order status to cancelled.
            Console.WriteLine($"${order.Amount:0.00} has been refunded to customer."); // 6. Display a message indicating that the amount is refunded.
            Console.WriteLine("Order has been cancelled successfully."); // 7. Display the status of the cancellation.
            Console.WriteLine();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// WRITING TO FILES METHODS ////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static void OrderFileWriting(List<Order> oList)
        {
            using (StreamWriter sw = new StreamWriter("../../../Order.csv"))
            {
                string heading = "OrderNo,OrderDateTime,Amount,Status,Tickets(scnNo/los/yob/pop)";
                sw.WriteLine(heading);
                foreach (Order o in oList)
                {
                    string data = "";
                    data += (o.OrderNo).ToString() + "," + (o.OrderDateTime).ToString() + "," + (o.Amount).ToString() + "," + o.Status + ",";
                    int count = 0;
                    foreach (Ticket t in o.TicketList)
                    {
                        if (t is Student)
                        {
                            Student s = (Student)t;
                            data += (s.Screening.ScreeningNo).ToString() + "/" + s.LevelOfStudy + "/" + "null" + "/" + "null";
                        }
                        else if (t is SeniorCitizen)
                        {
                            SeniorCitizen s = (SeniorCitizen)t;
                            data += (s.Screening.ScreeningNo).ToString() + "/" + "null" + "/" + (s.YearOfBirth).ToString() + "/" + "null";
                        }
                        else if (t is Adult)
                        {
                            Adult a = (Adult)t;
                            int popNumber = 0;
                            if (a.PopcornOffer == true)
                                popNumber = 1;
                            data += (a.Screening.ScreeningNo).ToString() + "/" + "null" + "/" + "null" + "/" + popNumber;
                        }
                        count++;
                        if (count <= o.TicketList.Count - 1)
                            data += "-";
                    }
                    sw.WriteLine(data);
                }
            }
        }
        static void ScreeningFileWriting(List<Screening> sList)
        {
            using (StreamWriter sw = new StreamWriter("../../../Screening.csv"))
            {
                string heading = "Date Time,Screening Type,Cinema Name,Hall Number,Movie Title";
                sw.WriteLine(heading);

                foreach (Screening s in sList)
                {
                    sw.WriteLine($"{s.ScreeningDateTime.ToString("dd/MM/yyyy hh:mmtt").ToUpper()},{s.ScreeningType},{s.Cinema.Name},{s.Cinema.HallNo},{s.Movie.Title}");
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// ADVANCED FEATURES METHODS ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // 3.1      RECOMMEND MOVIE BASED ON SALE OF TICKETS SOLD
        // AND
        // 3.3.1    TOP 3 MOVIES BASED ON TICKETS SOLD
        static void RecommendMovies(List<Screening> sList)
        {
            List<List<String>> ticketsSoldList = new List<List<string>>();
            foreach (Screening s in sList)
            {
                int ticketsSold = s.Cinema.Capacity - s.SeatsRemaining;  // Calculate tickets sold for each screening
                bool addedFlag = false;     // If movie title has been added into the ticketsSoldList, changes flag to true so that title is not added again.
                foreach (List<String> movie in ticketsSoldList)
                {
                    if (movie[0] == s.Movie.Title)
                    {
                        addedFlag = true;
                        movie[1] = ((Convert.ToInt32(movie[1]) + ticketsSold)).ToString(); // Cumulate tickets sold for each movie.
                    }
                }
                if (addedFlag == false)
                {
                    List<String> movie = new List<string> { s.Movie.Title, ticketsSold.ToString() }; // Add movie title if not in ticketsSoldList yet.
                    ticketsSoldList.Add(movie);
                }
            }
            Console.WriteLine("Recommended movies currently screening:"); // Recommend 3 movies based on sale of tickets sold.
            for (int i = 0; i < 3; i++)
            {
                List<string> topMovie = new List<string> { "", "-1" };      // Dummy list created.
                int numberOfMovies = ticketsSoldList.Count;
                int topMoviePosition = 0;
                for (int count = 0; count < numberOfMovies; count++)
                {
                    if (Convert.ToInt32(ticketsSoldList[count][1]) > Convert.ToInt32(topMovie[1]))  // Compare each movie to the movie with most tickets sold. 
                    {
                        topMovie = ticketsSoldList[count];  // When a higher number is detected, changes the topMovie as well as keep track of the index of the top movie in the list.
                        topMoviePosition = count;
                    }
                }
                ticketsSoldList.RemoveAt(topMoviePosition);     // Removes movie with highest ticket sold from the list so that the next top movie can be detected.
                Console.WriteLine($"{i + 1} — {topMovie[0]}");  // Displays top 3 movies.
            }
            Console.WriteLine();
        }

        // 3.2      DISPLAY AVAILABLE SEATS OF SCREENING SESSION IN DESCENDING ORDER (or other ways of sorting)
        static List<Screening> DisplayScreeningGroupByTitle(List<Screening> sList) // Displays Screenings grouped by the Movie Title ( currently used for DeleteMovieScreeningSession() )
        {
            List<String> movieTitleList = new List<string>();
            foreach (Screening s in sList)
            {
                if (movieTitleList.Contains(s.Movie.Title))
                {
                    continue;
                }
                else
                {
                    movieTitleList.Add(s.Movie.Title);
                }
            }

            List<Screening> sortedSList = new List<Screening>();
            foreach (string title in movieTitleList)
            {
                Console.WriteLine($"\nMovie Title: {title}");
                Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", "Screening No", "Screening Type", "Screening Date", "Screening Time", "Seats Remaining", "Cinema", "Hall No");               
                foreach (Screening s in sList)
                {
                    if (s.Movie.Title == title)
                    {
                        sortedSList.Add(s);
                           Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
                    }
                }
            }
            return sortedSList;
        }

        static void DisplayScreeningGroupByCinema(List<Screening> sList)  // Displays Screenings grouped by their Cinemas.
        {
            List<String> movieCinemaList = new List<string>();
            foreach (Screening s in sList)
            {
                if (movieCinemaList.Contains(s.Cinema.Name))
                    continue;
                else
                    movieCinemaList.Add(s.Cinema.Name);
            }

            List<Screening> sortedSList = new List<Screening>();
            foreach (string cinema in movieCinemaList)
            {
                Console.WriteLine($"\nCinema Name: {cinema}");
                Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", "Screening No", "Screening Type", "Screening Date", "Screening Time", "Seats Remaining", "Cinema", "Hall No");
                foreach (Screening s in sList)
                {
                    if (s.Cinema.Name == cinema)
                        Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
                    
                }
            }
        }

        static void DisplayScreeningGroupByDay(List<Screening> sList)  // Displays Screenings grouped by the Day of the Week.
        {
            List<String> movieDayList = new List<string>();
            foreach (Screening s in sList)
            {
                string day = s.ScreeningDateTime.DayOfWeek.ToString();
                if (movieDayList.Contains(day))
                    continue;
                else
                    movieDayList.Add(day);
            }

            foreach (string day in movieDayList)
            {               
                Console.WriteLine($"\nDay of the week: {day}");
                Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", "Screening No", "Screening Type", "Screening Date", "Screening Time", "Seats Remaining", "Cinema", "Hall No");
                
                foreach (Screening s in sList)
                {
                    if (s.ScreeningDateTime.DayOfWeek.ToString() == day)
                        Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);                   
                }
            }
        }

        static void DisplayScreeningOrderByAvailableSeats(List<Screening> sList)  // Main method for sorting in Descending and Ascending order, followed by grouping them up.
        {
            int groupBy;
            int orderBy;
            List<Screening> sortedSList = new List<Screening>(sList);
            while (true)
            {
                // Options for Sorting
                Console.WriteLine("Sort avaliable seats of screaning session in:");
                Console.WriteLine("[1] Decending Order");
                Console.WriteLine("[2] Ascending Order");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    orderBy = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (orderBy == 0)
                    {
                        Console.WriteLine("Returning to main menu...");
                        Console.WriteLine();
                        return;
                    }
                    else if (orderBy == 1 || orderBy == 2)
                    {
                        sortedSList.Sort();
                        if (orderBy == 1)
                            sortedSList.Reverse();
                        Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", "Screening No", "Screening Type", "Screening Date", "Screening Time", "Seats Remaining", "Cinema", "Hall No");
                        foreach (Screening s in sortedSList)
                        {
                            Console.WriteLine("{0, -14} {1, -16} {2, -16} {3, -16} {4, -17} {5, -15} {6, -10}", s.ScreeningNo, s.ScreeningType, (s.ScreeningDateTime).ToString("d"), (s.ScreeningDateTime).ToString("t"), s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
                        }
                        Console.WriteLine();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Option entered does not exsits, please try again!\n");
                        continue;
                    }              
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid format of option entered, Please enter an option number (e.g. 1 -> Descending Order).");
                    Console.WriteLine();
                }
            }
            // Options for Grouping
            while (true)
            {
                Console.WriteLine("Group avaliable seats of screaning session:");
                Console.WriteLine("[1] Group by Cinema");
                Console.WriteLine("[2] Group by Days of the week");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    groupBy = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (groupBy == 1)
                        DisplayScreeningGroupByCinema(sortedSList);
                    else if (groupBy == 2)
                        DisplayScreeningGroupByDay(sortedSList);
                    else if (groupBy == 0)
                    {
                        Console.WriteLine("Returning to main menu...");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Option entered does not exsits, please try again!");
                        continue;
                    }
                    Console.WriteLine();
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid format of option entered, Please enter an option number (e.g. 1 -> Group by Movie Title).");
                    Console.WriteLine();
                }
            }
        }

        // 3.3.4 USE WEB API TO ENHANCE THE SYSTEM
        // The plots and ratings of the current movies are extracted from https://rapidapi.com/apidojo/api/imdb8/
        static void MainMenu_MovieInformation(List<Movie> movieList)
        {
            // Menu for Movie Information. Options include retrieving the plot of current movies and the ratings for the current movies.
            while (true)
            {
                Console.WriteLine("==========================================");
                Console.WriteLine("             Movie Information            ");
                Console.WriteLine("==========================================");
                Console.WriteLine("[1] Plots of Current Movies");
                Console.WriteLine("[2] Ratings for Current Movies");
                Console.WriteLine();
                Console.WriteLine("[0] Return to Main Menu");
                Console.WriteLine("------------------------------------------");
                Console.Write("Please select an option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (option == 1)
                    {
                        MoviePlot(movieList).Wait();
                        break;
                    }
                    else if (option == 2)
                    {
                        MovieRating(movieList).Wait();
                        break;
                    }
                    else if (option == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please select from the options available.");
                        Console.WriteLine();
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter the option number (e.g. 1 -> Plots for Current Movies).");
                }
            }
        }

        // Method to obtain the Plot of the selected Movie.
        static async Task MoviePlot(List<Movie> mList)
        {
            while (true)
            {
                Movie movie = GetMovieSelected(mList);
                Console.WriteLine();
                if (movie == null)
                    return;
                string movieTitle = movie.Title;
                string lowerMovieTitle = movieTitle.ToLower();
                string movieTitleURL = HttpUtility.UrlPathEncode(lowerMovieTitle);      // String is encoded for RequestUri
                string ttconst = null;
                // Get IMDB ttconst of Movie
                using (HttpClient client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"https://imdb8.p.rapidapi.com/title/find?q={movieTitleURL}"),
                        Headers = {
                            { "x-rapidapi-host", "imdb8.p.rapidapi.com" },
                            { "x-rapidapi-key", "815c88557amsh5499a7f6af798f1p18898bjsnff8fbeee1bd5" }
                        }
                    };
                    Console.WriteLine("Getting data... Please wait a moment.");
                    HttpResponseMessage response = await client.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        FindMovie moviedata = JsonConvert.DeserializeObject<FindMovie>(data);
                        bool successFlag = false;
                        foreach (Result r in moviedata.results)
                        {
                            if (r.title == movieTitle)
                            {
                                successFlag = true;
                                ttconst = r.id;
                                ttconst = ttconst.Substring(7);
                                ttconst = ttconst.Trim('/');
                                break;
                            }
                        }
                        if (successFlag == false)
                        {
                            Console.WriteLine("Movie data cannot be found. Returning to Movie Information Menu.");
                            Console.WriteLine();
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error retrieving data. Returning to Movie Information Menu.");
                        Console.WriteLine();
                        return;
                    }
                }
                // Get Plot of Movie using ttconst obtained
                using (HttpClient client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"https://imdb8.p.rapidapi.com/title/get-plots?tconst={ttconst}"),
                        Headers = {
                            { "x-rapidapi-host", "imdb8.p.rapidapi.com" },
                            { "x-rapidapi-key", "815c88557amsh5499a7f6af798f1p18898bjsnff8fbeee1bd5" }
                        }
                    };
                    Console.WriteLine("Getting movie plot... Please wait a moment.");
                    HttpResponseMessage response = await client.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        GetPlot plotdata = JsonConvert.DeserializeObject<GetPlot>(data);
                        Console.WriteLine();  // Display plot
                        Console.WriteLine("------------------------------------------");
                        Console.WriteLine();
                        Console.WriteLine("Movie Title: " + movieTitle);
                        Console.WriteLine();
                        int i = 0;
                        Console.WriteLine($"Plot {i + 1} of {plotdata.plots.Length}");         
                        Console.WriteLine(plotdata.plots[i].text);
                        Console.WriteLine();
                        Console.WriteLine("------------------------------------------");
                        while (true)
                        {
                            Console.WriteLine();
                            Console.WriteLine("[1] Different Plot");  // Multiple plots are available.
                            Console.WriteLine("[2] Choose Another Movie and View Plot");
                            Console.WriteLine("[3] Return to main menu");
                            Console.WriteLine("------------------------------------------");
                            Console.Write("Please select an option: ");
                            try
                            {
                                int option = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine();
                                if (option == 1)
                                {
                                    // Goes to next plot. Goes back to first plot if user is already at the final plot.
                                    if (i != plotdata.plots.Length - 1)
                                        i += 1;
                                    else
                                        i = 0;
                                    Console.WriteLine("------------------------------------------");
                                    Console.WriteLine();
                                    Console.WriteLine("Movie Title: " + movieTitle);
                                    Console.WriteLine();
                                    Console.WriteLine($"Plot {i + 1} of {plotdata.plots.Length}");
                                    Console.WriteLine(plotdata.plots[i].text);
                                    Console.WriteLine();
                                    Console.WriteLine("------------------------------------------");
                                }
                                else if (option == 2)
                                {
                                    Console.WriteLine();
                                    break;
                                }
                                else if (option == 3)
                                {
                                    return;
                                }
                                else
                                {
                                    Console.WriteLine("Please select from the options available.");
                                    Console.WriteLine();
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter the option number (e.g. 1 -> Different Plot).");
                                Console.WriteLine();
                            }
                        }
                    }
                }               
            }
        }

        // Method to obtain Ratings for selected Movie.
        static async Task MovieRating(List<Movie> mList)
        {
            while (true)
            {
                Movie movie = GetMovieSelected(mList);
                Console.WriteLine();
                if (movie == null)
                    return;
                string movieTitle = movie.Title;
                string lowerMovieTitle = movieTitle.ToLower();
                string movieTitleURL = HttpUtility.UrlPathEncode(lowerMovieTitle);   // String is encoded for RequestUri
                string ttconst = null;
                // Get IMDB ttconst of Movie
                using (HttpClient client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"https://imdb8.p.rapidapi.com/title/find?q={movieTitleURL}"),
                        Headers = {
                            { "x-rapidapi-host", "imdb8.p.rapidapi.com" },
                            { "x-rapidapi-key", "815c88557amsh5499a7f6af798f1p18898bjsnff8fbeee1bd5" }
                        }
                    };
                    Console.WriteLine("Getting data... Please wait a moment.");
                    HttpResponseMessage response = await client.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        FindMovie moviedata = JsonConvert.DeserializeObject<FindMovie>(data);
                        bool successFlag = false;
                        foreach (Result r in moviedata.results)
                        {
                            if (r.title == movieTitle)
                            {
                                successFlag = true;
                                ttconst = r.id;
                                ttconst = ttconst.Substring(7);
                                ttconst = ttconst.Trim('/');
                                break;
                            }
                        }
                        if (successFlag == false)
                        {
                            Console.WriteLine("Movie data cannot be found. Returning to Movie Information Menu.");
                            Console.WriteLine();
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error retrieving data. Returning to Movie Information Menu.");
                        Console.WriteLine();
                        return;
                    }
                }
                // Get rating of movie using ttconst obtained
                using (HttpClient client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"https://imdb8.p.rapidapi.com/title/get-ratings?tconst={ttconst}"),
                        Headers = {
                            { "x-rapidapi-host", "imdb8.p.rapidapi.com" },
                            { "x-rapidapi-key", "815c88557amsh5499a7f6af798f1p18898bjsnff8fbeee1bd5" }
                        }
                    };
                    Console.WriteLine("Getting movie rating... Please wait a moment.");
                    HttpResponseMessage response = await client.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        GetRating ratingData = JsonConvert.DeserializeObject<GetRating>(data);
                        Console.WriteLine();
                        // Display rating of movie.
                        Console.WriteLine("Movie Title    : " + movieTitle);
                        Console.WriteLine("Rating         : " + ratingData.rating);
                        Console.WriteLine("No. of Ratings : " + ratingData.ratingCount);
                        while (true)
                        {
                            Console.WriteLine();
                            Console.WriteLine("[1] View Another Movie Rating");  // User can see the rating of another movie.
                            Console.WriteLine("[2] Return to main menu");
                            Console.WriteLine("------------------------------------------");
                            Console.Write("Please select an option: ");
                            try
                            {
                                int option = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine();
                                if (option == 1)
                                {
                                    Console.WriteLine();
                                    break;
                                }
                                else if (option == 2)
                                {
                                    return;
                                }
                                else
                                {
                                    Console.WriteLine("Please select from the options available.");
                                    Console.WriteLine();
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter the option number (e.g. 1 -> Different Plot).");
                                Console.WriteLine();
                            }
                        }
                    }
                }
            }
        }
    }   
}
