﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Order
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; } = new List<Ticket>();
        public Order() { }
        public Order(int oN, DateTime oDT)
        {
            OrderNo = oN;
            OrderDateTime = oDT;
        }
        public void AddTicket(Ticket t)
        {
            TicketList.Add(t);
        }
        public override string ToString()
        {
            string Tickets = "Tickets: ";
            foreach (Ticket t in TicketList)
            {
                if (t is SeniorCitizen)
                {
                    SeniorCitizen seniorCitizen = (SeniorCitizen)t;
                    Tickets += $"[Senior Citizen, {seniorCitizen.YearOfBirth}, ${seniorCitizen.CalculatePrice():0.00}] ";
                }
                else if (t is Student)
                {
                    Student student = (Student)t;
                    Tickets += $"[Student, {student.LevelOfStudy}, ${student.CalculatePrice():0.00}] ";
                }
                else if (t is Adult)
                {
                    Adult adult = (Adult)t;
                    Tickets += $"[Student, {(adult.PopcornOffer ? "Yes" : "No")}, ${adult.CalculatePrice():0.00}] ";
                }
            }
            Tickets = Tickets.Trim();
            return $"Order No: {OrderNo}\nOrder Date & Time: {OrderDateTime}\nAmount: {Amount}\nStatus: {Status}\n{Tickets}";
        }
    }
}
