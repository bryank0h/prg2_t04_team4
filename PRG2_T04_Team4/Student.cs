﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Student : Ticket
    {
        public string LevelOfStudy { get; set; }
        public Student() { }
        public Student(Screening sc, string los) : base(sc)
        {
            LevelOfStudy = los;
        }
        public override double CalculatePrice()
        {
            string dayOfScreening = Screening.ScreeningDateTime.DayOfWeek.ToString();
            string[] weekdayArray = { "Monday", "Tuesday", "Wednesday", "Thursday" };
            bool weekdayFlag = weekdayArray.Contains(dayOfScreening); // If weekday, flag is true. If weekend, flag is false.
            int daysDifference = Screening.ScreeningDateTime.Subtract(Screening.Movie.OpeningDate).Days;

            // Check if it is the first 7 days of movie opening date. If so, charge adult price.
            if (daysDifference <= 7)
            {
                // Check if it is the weekday or weekend.
                if (weekdayFlag)
                {
                    // Check screening type.
                    if (Screening.ScreeningType == "3D")
                        return 11.00;
                    else
                        return 8.50;
                }
                else
                {
                    // Check screening type.
                    if (Screening.ScreeningType == "3D")
                        return 14.00;
                    else
                        return 12.50;
                }
            }
            else
            {
                // Check if it is the weekday or weekend.
                if (weekdayFlag)
                {
                    // Check screening type.
                    if (Screening.ScreeningType == "3D")
                        return 8.00;
                    else
                        return 7.00;
                }
                else
                {
                    // Check screening type.
                    if (Screening.ScreeningType == "3D")
                        return 14.00;
                    else
                        return 12.50;
                }
            }
        }
        public override string ToString()
        {
            return base.ToString() + $"\nLevel of Study: {LevelOfStudy}\nPrice of ticket: {CalculatePrice()}";
        }
    }         
}
