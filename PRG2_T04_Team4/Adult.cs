﻿//============================================================
// Student Number : S10223617, S10222425
// Student Name : Bryan Koh, Lee Wei Jun Nicholas
// Module Group : T04
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T04_Team4
{
    class Adult : Ticket
    {
        public bool PopcornOffer { get; set; }
        public Adult() { }
        public Adult(Screening sc, bool p) : base(sc)
        {
            PopcornOffer = p;
        }
        public override double CalculatePrice()
        {
            double price;
            string dayOfScreening = Screening.ScreeningDateTime.DayOfWeek.ToString();
            string[] weekdayArray = { "Monday", "Tuesday", "Wednesday", "Thursday" };
            bool weekdayFlag = weekdayArray.Contains(dayOfScreening); // If weekday, flag is true. If weekend, flag is false.

            // Check if it is the weekday or weekend.
            if (weekdayFlag)
            {
                // Check screening type.
                if (Screening.ScreeningType == "3D")
                    price = 11.00;
                else
                    price = 8.50;
            }
            else
            {
                // Check screening type.
                if (Screening.ScreeningType == "3D")
                    price = 14.00;
                else
                    price = 12.50;
            }
            // Check if customer agreed to the popcorn offer.
            if (PopcornOffer == true)
                price += 3.00;
            return price;
        }
        public override string ToString()
        {
            return base.ToString() + $"\nPopcorn Offer: {(PopcornOffer ? "Yes" : "No")}\nPrice of ticket: {CalculatePrice()}";
        }
    }
}
